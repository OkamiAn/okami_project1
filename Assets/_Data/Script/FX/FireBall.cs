using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : SaiMonoBehaviour
{
    public float time = 1.0f;
    public Vector3 dir=new Vector3(0,0,0);
    public GameObject Test;
    [SerializeField] protected string tagTarget = "Enemy";
    [SerializeField] protected float speed = 9f;
    [SerializeField] protected Transform model;
    [SerializeField] protected string hitFx ="";
    [SerializeField] protected string moveFx = "";
    [SerializeField] protected DamageSender damageSender;
    [SerializeField] protected Collider _collider;
    [SerializeField] protected bool isSkillMove;

    void Start()
    {
        Destroy(gameObject, 5f);
        if (isSkillMove) StartCoroutine(SpawnTest());  
    }
    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadModel();
        this.LoadDamageSender();
    }
    protected virtual void LoadDamageSender()
    {
        if (this.damageSender != null) return;
        this.damageSender = transform.Find("DamageSender").GetComponent<DamageSender>();
        this._collider = GetComponent<Collider>();
        this._collider.isTrigger = true;
        Debug.Log(transform.name + ": LoadDamageSender", gameObject);
    }

    protected virtual void LoadModel()
    {
        if (this.model != null) return;
        this.model = transform.Find("Model");
        Debug.Log(transform.name + ": LoadModel", gameObject);
    }

    protected override void Update()
    {
        if (isSkillMove)
        {
            if (transform.localScale.x > 0)
                dir = transform.TransformDirection(Vector3.right);
            else
                dir = transform.TransformDirection(Vector3.left);
            transform.position+=(dir * 10 * Time.deltaTime);
        }
            
    }
    IEnumerator SpawnTest()
    {
        while (true)
        {
            Transform fx = FXManager.instance.Spawn(moveFx);
            fx.position = transform.position;
            fx.localScale = transform.localScale;
            fx.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.2f);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        

        if (other.tag == tagTarget)
        {
            
            DamageReceiver damageReceiver = other.GetComponentInChildren<DamageReceiver>();
            if (!damageReceiver) return;

            if (this.hitFx!=null)
            {
                Transform fx = FXManager.instance.Spawn(hitFx);

                Vector3 pos = other.transform.position;
                fx.position = pos;
                fx.gameObject.SetActive(true);

            }
            damageReceiver.Deduct(this.damageSender.Damage());

        }
        
        if (other.tag == "Wall") Destroy(gameObject);

    }
    //void OnControllerColliderHit(ControllerColliderHit hit)
    //{
    //    Rigidbody body = hit.collider.attachedRigidbody;

      //  Debug.Log(hit.gameObject.name);
  //  }
}
