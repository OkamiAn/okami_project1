using UnityEngine;

public class SaiMonoBehaviour : MonoBehaviour
{
    protected virtual void Awake()
    {
        this.LoadComponents();
    }

    protected virtual void Reset()
    {
        this.LoadComponents();
    }

    protected virtual void LoadComponents()
    {
        //For override   
    }
    protected virtual void Update() 
    { 
    }

    protected virtual void FixedUpdate()
    {

    }
}
