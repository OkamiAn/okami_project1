using UnityEngine;

public class DamageSender : SaiMonoBehaviour
{
    [Header("DamageSender")]
    [SerializeField] protected int damage = 1;

    public virtual int Damage()
    {
        return this.damage;
    }
}