using System.Collections;
using UnityEngine;

public abstract class BaseCombat : SaiMonoBehaviour
{
    [Header("BaseCombat")]

    public float attacking = 0f;
    public bool canAttack = true;
    public float attackTimer = 0f;
    public float attackSpeed = 1f;

    protected abstract Animator Animator();
    public abstract void SpawnSkill();
    public abstract BaseController BaseController();

    protected override void Update()
    {

        this.Attacking();
    } 

    protected virtual void Attacking()
    {
        this.Animator().SetBool("Attacking", this.IsAttacking());
        if (this.IsAttacking()) StartCoroutine(Attacked());
    }

    protected virtual IEnumerator Attacked()
    {
        this.canAttack = false;
        yield return new WaitForSeconds(.3f);
        if (this.Animator().GetCurrentAnimatorStateInfo(0).IsName("CharIAttack"))
            this.SpawnSkill();
        yield return new WaitForSeconds(attackSpeed);
        this.canAttack = true;
    }
   

    /// <summary>
    /// Call from Invoke
    /// </summary>
    public virtual void AttackFinish()
    {
        this.canAttack = false;
        this.attackTimer = 0;
    }

    public virtual bool IsAttacking()
    {
        return this.attacking != 0 && this.canAttack;
    }
}