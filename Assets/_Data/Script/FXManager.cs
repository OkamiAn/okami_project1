using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : SaiMonoBehaviour
{
    public static FXManager instance;

    [SerializeField] protected List<Transform> fxs = new List<Transform>();

    protected override void Awake()
    {
        base.Awake();
        if (FXManager.instance != null) Debug.LogError("Only 1 FXManager allow");
        FXManager.instance = this;

        this.HideAll();
    }

    protected override void LoadComponents()
    {
        this.LoadFXS();
    }

    protected virtual void LoadFXS()
    {
        if (this.fxs.Count > 0) return;
        foreach (Transform child in transform)
        {
            this.fxs.Add(child);
        }
        Debug.Log(transform.name + ": LoadFXS", gameObject);
    }

    protected virtual void HideAll()
    {
        foreach (Transform fx in this.fxs)
        {
            fx.gameObject.SetActive(false);
        }
    }

    public virtual Transform Spawn(string fxName)
    {
        if(this.fxs.Count<=0) return null;

        Transform fx;
        int i = 0;

        for (;;)
        {
            if(this.fxs[i].name == fxName)
            {
                fx = Instantiate(this.fxs[i], transform.parent);
                break;
            }
            i++;
        }
        return fx;
    }
}
