using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : BaseCombat
{

    public EnemyController EnemyController;

    public Vector3 test;
    public bool inRangeAtk;
    public float rangeAtk=3;
    [SerializeField] protected float range;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadMonsterCtrl();
    }
    protected virtual void LoadMonsterCtrl()
    {
        if (this.EnemyController != null) return;
        this.EnemyController = transform.parent.GetComponent<EnemyController>();
        Debug.Log(transform.name + ": LoadMonsterCtrl", gameObject);
    }
    protected override void Update()
    {
        base.Update();
        this.TargetInRange();
    }

    private void TargetInRange()
    {
        Transform target = this.EnemyController.findTarget.Target();
        if (!this.EnemyController.findTarget.IsTargetAvail()) return;

        range = Vector3.Distance(target.position,transform.position);

        if (range <= rangeAtk) this.inRangeAtk = true;
        else this.inRangeAtk = false;

    }

    protected override void Attacking()
    {  
        if (this.IsAttacking()) 
        {
            this.Animator().SetBool("Attacking", true);
            StartCoroutine(Attacked());
        }
    }

    protected override IEnumerator Attacked()
    {
        this.canAttack = false;
        
        yield return new WaitForSeconds(.9f);
        if (EnemyController.animator.GetCurrentAnimatorStateInfo(0).IsName("CharIAttack"))
            this.SpawnSkill();
        yield return new WaitForSeconds(0.7f);
        this.Animator().SetBool("Attacking", false);
        this.canAttack = true;
    }

    public override void SpawnSkill()
    {
        Transform fx = FXManager.instance.Spawn("BossHit");
        fx.position = this.EnemyController.transform.position;
        fx.localScale = this.EnemyController.model.localScale;
        fx.gameObject.SetActive(true);
    }

    protected override Animator Animator()
    {
       return this.EnemyController.animator;
    }

    public override BaseController BaseController()
    {
        return this.EnemyController.monsterMovement;
    }

    public override bool IsAttacking()
    {
        return this.inRangeAtk && this.canAttack;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 3f);
    }

}
