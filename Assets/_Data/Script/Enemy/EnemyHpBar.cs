using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHpBar : SaiMonoBehaviour
{

    [SerializeField] protected Text hpString;
    [SerializeField] protected int hpMax;
    [SerializeField] protected int curHp;
    [SerializeField] protected DamageReceiver DamageReceiver;
    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.hpString = this.transform.Find("Luong").GetComponent<Text>();
        this.DamageReceiver = this.transform.parent.GetComponentInChildren<DamageReceiver>();
    }

    protected override void Awake()
    {
        this.hpMax = DamageReceiver.hp;
        this.curHp = DamageReceiver.hp;
    }

    protected override void Update()
    {
        this.curHp = this.DamageReceiver.hp;
        this.hpString.text = this.curHp + "/" + this.hpMax;
    }
}
