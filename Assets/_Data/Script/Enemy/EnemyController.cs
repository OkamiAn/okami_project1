using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : SaiMonoBehaviour
{
    public EnemyMove monsterMovement;
    public EnemyCombat enemyCombat;
    public FindTarget findTarget;
    public CharacterController characterController;
    public Transform model;
    public Animator animator;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadMonsterMovement();
    }

    protected virtual void LoadMonsterMovement()
    {
        if (this.monsterMovement != null) return;
        this.model = transform.Find("Model");
        this.animator = this.model.GetComponentInChildren<Animator>();
        this.findTarget=transform.Find("FindTarget").GetComponent<FindTarget>();
        this.monsterMovement = transform.Find("EnemyMove").GetComponent<EnemyMove>();
        this.enemyCombat = transform.Find("EnemyCombat").GetComponent<EnemyCombat>();
        this.characterController = GetComponent<CharacterController>();
        Debug.Log(transform.name + ": LoadEnemyMove", gameObject);
    }
}
