﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : BaseController
{
   // float latestDirectionChangeTime = 0f;
    public EnemyController EnemyController;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadMonsterCtrl();
        this.speed = 2f;
        this.lookAtMouse = false;
    }
  
    protected virtual void LoadMonsterCtrl()
    {
        if (this.EnemyController != null) return;
        this.EnemyController = transform.parent.GetComponent<EnemyController>();
        Debug.Log(transform.name + ": LoadMonsterCtrl", gameObject);
    }



    
    protected override Animator Animator()
    {
        return this.EnemyController.animator;
    }

    protected override CharacterController CharacterController()
    {
        return this.EnemyController.characterController;
    }

    protected override Transform Model()
    {
        return this.EnemyController.model;
    }
    protected override void Moving()
    {


        // Nếu đang trong khoảng cách có thể tấn công thì không di chuyển nữa.

        if (this.EnemyController.enemyCombat.inRangeAtk) 
        {
            this.moveHorizontal = 0;
            this.movement.x = 0;
            return;
        }
        base.Moving();
    }

    protected override void TurnByMovement()
    {
        this.isTurnRight = true;
        Transform target = this.EnemyController.findTarget.Target();

        if (!this.EnemyController.findTarget.IsTargetAvail()) return;




        this.mouseToChar = target.position - transform.parent.position;
        if (this.mouseToChar.x != 0) this.lastDirection = this.mouseToChar.x;

        if (this.lastDirection < 0)
        {
            this.moveHorizontal = -1;
            this.isTurnRight = false;
        }
        else this.moveHorizontal = 1;
    }

}
