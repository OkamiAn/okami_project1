using UnityEngine;

public class PlayerMovement : BaseController
{
    public PlayerController playerController;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadPlayerController();
        this.speed = 3f;
    }

    protected virtual void LoadPlayerController()
    {
        if (this.playerController != null) return;
        this.playerController = transform.parent.GetComponent<PlayerController>();
        Debug.Log(transform.name + ": LoadPlayerController", gameObject);
    }

    protected override CharacterController CharacterController()
    {
        return this.playerController.characterController;
    }

    protected override Transform Model()
    {
        return this.playerController.playerModel;
    }

    protected override Animator Animator()
    {
        return this.playerController.animator;
    }

    protected override void Moving()
    {
       // if (PlayerController.instance.playerCombat.IsAttacking())
        //{
        //    this.movement.x = 0;
        //    return;
        //}
        base.Moving();
    }
    protected override void UpdateMoving()
    {
        base.UpdateMoving();

        PlayerController.instance.fxMove.gameObject.SetActive(this.IsMoving());
    }
}
