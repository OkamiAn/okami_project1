using UnityEngine;
using System.Collections;

public class PlayerCombat : BaseCombat
{
    public PlayerController controller;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadPlayerController();
    }

    protected virtual void LoadPlayerController()
    {
        if (this.controller != null) return;
        this.controller = transform.parent.GetComponent<PlayerController>();
        Debug.Log(transform.name + ": LoadPlayerController", gameObject);
    }


    public override void SpawnSkill()
    {
        Transform fx = FXManager.instance.Spawn("KiemKhi");
        fx.position = PlayerController.instance.skillSpawn.position;
        fx.localScale = PlayerController.instance.playerModel.localScale;
        fx.gameObject.SetActive(true);
    }


    protected override Animator Animator()
    {
        return this.controller.animator;
    }

    public override BaseController BaseController()
    {
        return this.controller.playerMovement;
    }
}
