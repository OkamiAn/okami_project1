using UnityEngine;

public class PlayerDamageReceiver: DamageReceiver
{
    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.hp = 999;
    }
}