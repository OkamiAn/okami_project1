using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHpBar : SaiMonoBehaviour
{
    [SerializeField] protected Text hpString;
    [SerializeField] protected int hpMax;
    [SerializeField] protected int curHp;
    [SerializeField] protected PlayerDamageReceiver DamageReceiver;
    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.hpString = this.transform.Find("Luong").GetComponent<Text>();
        this.DamageReceiver = this.transform.parent.GetComponentInChildren<PlayerDamageReceiver>();
    }

    protected override void Awake()
    {
        this.hpMax = DamageReceiver.hp;
        this.curHp = DamageReceiver.hp;
    }

    protected override void Update()
    {
        this.curHp = DamageReceiver.hp;
        this.hpString.text = curHp + "/" + hpMax;
    }

}
